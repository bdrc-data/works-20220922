@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:WA0RK0127 {
    bda:LG0BDSFKVMEH92E3KU
        a                   adm:UpdateData ;
        adm:logAgent        "IATTC/scripts/write_rdf.py" ;
        adm:logDate         "2024-11-14T10:25:47.909421"^^xsd:dateTime ;
        adm:logMessage      "import data from the ATII project"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:WA0RK0127  a        adm:AdminData ;
        adm:adminAbout      bdr:WA0RK0127 ;
        adm:logEntry        bda:LG0BDSFKVMEH92E3KU ;
        adm:metadataLegal   bda:LD_rKTs_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CRWA0RK0127_ATII0
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P8266 ;
        bdo:role            bdr:R0ER0026 .
    
    bdr:ID9D1B1C_WA0RK0127
        a                   bdr:RefrKTsK ;
        rdf:value           "127" .
    
    bdr:NTWA0RK0127_ATII0
        a                   bdo:Note ;
        bdo:noteText        "Attribution information contributed by the Authors and Translators Identification Initiative (ATII) project in collaboration with the Khyentse Center at Universität Hamburg."@en .
    
    bdr:WA0RK0127  a        bdo:Work ;
        bf:identifiedBy     bdr:ID9D1B1C_WA0RK0127 ;
        bdo:creator         bdr:CRWA0RK0127_ATII0 ;
        bdo:isRoot          true ;
        bdo:language        bdr:LangBo ;
        bdo:note            bdr:NTWA0RK0127_ATII0 ;
        bdo:workHasParallelsIn  bdr:WA0RKI0127 ;
        owl:sameAs          <http://purl.rkts.eu/resource/WKT0127> ;
        skos:altLabel       "'phags pa chos thams cad kyi rang bzhin mnyam ba nyid rnam par spros pa'-i t-ing nge 'dzin kyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad kyi rang bzhin mnyam ba nyid rnam par spros pa'-i t-ing nge 'dzin kyi rgyal po zhes bya ba theg pa chen po 'i mdo/"@bo-x-ewts , "'phags pa chos thams cad kyi rang bzhin mnyam ba nyid rnam par spros pa ting nge 'dzin kyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad kyi rang bzhin mnyam pa nyid du rnam par spros pa ting nge 'dzin gyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad kyi rang bzhin mnyam pa nyid rnam par spros pa'i ting nge 'dzin gyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad kyi rang bzhin mnyan pa nyid rnam par spros pa ting nge 'dzin gyi rgyal po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad kyi rang bzhin mxf1;am ba xf1;id rnam par spros pa ting nge 'dzin kyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad rang bzhin mnyam ba nyid rnam par spros pa ting nge 'dzind gyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa chos thams cad rang bzhin mnyam ba nyid rnam par spros pa ting nge 'dzin kyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts , "'phags pa ting nge 'dzind kyi rgyal po'i mdo/"@bo-x-ewts , "'phags pa ting nge 'dzin gyi rgyal po/"@bo-x-ewts , "chos thams cad kyi rang bzhin mnyam ba nyid rnam par spros pa'i ting nge 'dzind ci snyed pa/"@bo-x-ewts , "chos thams cad kyi rang bzhin mnyam pa nyid rnam par spros pa'i ting nge 'dzin gyi dbang po zhes bya ba/"@bo-x-ewts , "chos thams cad kyi rang bzhin mxf1;am ba xf1;id rnam par spros pa'i ting nge 'dzind ci sxf1;ed pa/"@bo-x-ewts , "ting 'dzin rgyal po'i mdo/"@bo-x-ewts , "ᠬᠣᠳᠣᠬ ᠳᠣ ᠬᠠᠮᠣᠬ ᠨᠣᠮ ᠦᠳ ᠦᠨ ᠮᠥᠨ ᠴᠢᠨᠠᠷ ᠳᠡᠭᠰᠢ ᠰᠠᠴᠠ ᠳᠡᠶᠢᠨ ᠪᠥᠭᠡᠳ ᠡᠳᠥᠭᠥᠯᠥᠭᠰᠡᠨ ᠰᠠᠮᠠᠳᠢᠰ ᠦᠨ ᠬᠠᠭᠠᠨ ᠨᠡᠷᠡᠳᠥ ᠶᠡᠭᠡ ᠭᠥᠯᠭᠡᠨ ᠰᠣᠳᠣᠷ"@cmg-Mong , "qutug tu qamug nom ud un mön cinar tegsi saca teyin böged edügülügsen samadis un qagan neretü yeke kölgen sudur"@cmg-x-poppe ;
        skos:prefLabel      "'phags pa chos thams cad kyi rang bzhin mnyam pa nyid rnam par spros pa ting nge 'dzin gyi rgyal po zhes bya ba theg pa chen po'i mdo/"@bo-x-ewts .
}
